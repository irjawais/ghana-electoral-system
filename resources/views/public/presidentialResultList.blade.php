<!DOCTYPE html>
<html lang="en">
<head>
  <title>{{$election->name}}</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>{{$election->name}}</h2>
  <table class="table table-hover">
    <thead>
      <tr>
        <th>Polling Station</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($electionResult as $result)
      <tr>
        <td>{{$result->name}}</td>
        @if($result->verify_by_constituency == 1)
            <td style="background-color: green;">Verified</td>
        @else
            <td style="background-color: red;">Unverified</td>
        @endif
      </tr>
      @endforeach


    </tbody>
  </table>
</div>

</body>
</html>
