@extends('admin.layouts.app')

@section('content')

<div class="row tile_count">
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-user"></i> Regions</span>
          <div class="count" style="font-size:25px">{{number_format($total_region)}}</div>
          <span class="count_bottom"><i class="green"> </i></span>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-clock-o"></i>   Constituencies</span>
          <div class="count"style="font-size:25px">{{number_format($total_constituency)}}</div>
          <span class="count_bottom"><i class="green"></i> </span>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-user"></i>   Electoral Areas</span>
          <div class="count "style="font-size:25px">{{number_format($total_electoralArea)}}</div>
          <span class="count_bottom"><i class="green"> </i> </span>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i>  Polling Stations</span>
            <div class="count "style="font-size:25px">{{number_format($total_pollingStation)}}</div>
            <span class="count_bottom"><i class="green"> </i> </span>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Total Voters</span>
                <div class="count "style="font-size:25px">{{number_format($total_voters)}}</div>
                <span class="count_bottom"><i class="green"> </i> </span>
            </div>

        {{-- <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-user"></i> Total Presidential Candidates </span>
          <div class="count">4,567</div>
          <span class="count_bottom"><i class="red"> </i> </span>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-user"></i> Total Parliamentary Candidates</span>
          <div class="count">2,315</div>
          <span class="count_bottom"><i class="green"> </i> </span>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-user"></i> Total District Assembly Candidates </span>
          <div class="count">7,325</div>
          <span class="count_bottom"><i class="green"> </i> </span>
        </div> --}}
        @foreach ($userTypeDetail as $detail)
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i>  {{$detail->name}}s</span>
                <div class="count"style="font-size:25px">{{$detail->user_type__count}}</div>
                <span class="count_bottom"><i class="green"> </i> </span>
              </div>
        @endforeach
      </div>

<div id="chartContainer" style="height: 370px; width: 100%;"></div>

@endsection
@section("script")

<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>


<script>
    var chart;
    window.onload = function () {

    chart = new CanvasJS.Chart("chartContainer", {
    animationEnabled: true,
    theme: "light2",
    title: {
        text: "Presidential Election Result"
    },
    axisY: {
        suffix: "%",
        scaleBreaks: {
          //  autoCalculate: true
        }
    },
    data: [{
        type: "column",
        yValueFormatString: "#,###.##\"%\"",

       // yValueFormatString: "#,##0\"%\"",
        indexLabel: "{y}",
        indexLabelPlacement: "inside",
        indexLabelFontColor: "white",
        dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
    }]
});
chart.render();

}

    var data;
     function getData(){

        var _token = $('input[name="_token"]').val();

           $.ajax({
               url: "{{route('SuperAdmin.presidentialResultAjax')}}",
               type: 'POST',
               data: {
                   _token : _token
               },
               success: function (data1) {
                chart.options.data[0].dataPoints  = data1;
                chart.render()
               }
           });


    }
    setInterval(function(){
        getData()
    }, 3000);


    </script>

        @endsection
