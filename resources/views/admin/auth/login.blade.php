@extends('admin.layouts.apps')

@section('content')
    {{-- <div class="container" >

      <div class="row">
        <div class="col-lg-4 col-md-6 ml-auto mr-auto">
                <br><br><br><br>
          <div class="card card-login">
            <form class="" method="POST" action="{{ route('login') }}">
                    @csrf
              <div class="card-header card-header-primary text-center">
                <h4 class="card-title">Login</h4>
               </div>
              <p class="description text-center">{{$config['name']}}</p>
              <div class="card-body">
               <div class="input-group">
                        @if ($errors->has('email'))
                            <div class="alert alert-danger">
                                    {{ $errors->first('email') }}
                            </div>
                        @endif
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">mail</i>
                    </span>

                  </div>


                  <input id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email...">
                </div>
                <div class="input-group">
                        @if ($errors->has('password'))
                        <br>
                        <div class="alert alert-danger">
                                {{ $errors->first('password') }}
                        </div>
                        @endif
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">lock_outline</i>
                    </span>
                  </div>
                  <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password...">

                </div>
              </div>
              <div class="footer text-center">
                  <button  class="btn btn-primary btn-link btn-wd btn-lg" type="submit">Submit</button>
                  <br>
                  <a class=" btn btn-primary btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div> --}}

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Super Admin Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('SuperAdmin.login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                {{-- @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif --}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
